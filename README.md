Edilex Chatbot
==============================

The goal of this project is to create a recommendation chatbot
that aids in writing legal contracts in French language.

Chatbot v.0 (beta) : chatbot extracts relevant answers/clauses from the static KB (Knowledge Base).

Chatbot v.1 : performance improvement (BERT, algorithms).

Chatbot v.2 : incorporation of user profile, user score.

Chatbot v.3 : Uses reinforcement learning to make recommendations


Pre-processing of texts happens in the /src directory.

Data exploration and model training - in the /notebooks directory.

Web development (API and/or front-end): - in the /flask_app directory.


How to use this repository:
===========================

1. Clone this repository

    `git clone https://<your_user_name>@bitbucket.org/Edilex-Project/chatbot-edilex.git`

2. Change into the project directory:

    `cd chatbot-edilex`

3. Create an environment by running:

      `conda env create --name edilex-env -f environment.yml`

4. Activate this new environment:

      `conda activate edilex-env`

5. Install an additional package with Spacy's French-language model. (The line before "python" makes sure to point to your environment's version of Python):

      `/User/<your_user_name>/miniconda3/envs/edilex-env/bin/python -m spacy download fr_core_news_sm`

6. To run the web app, in a terminal window go to the /flask_app folder and type 'python app.py'. 


Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
