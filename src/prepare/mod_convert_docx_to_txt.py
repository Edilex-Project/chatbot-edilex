# -*- coding: utf-8 -*-

#*******************************************************************************
# This module contains functions to convert MS Word documents to text files
# (with the help of the "docx2txt" Python package), and to insert these text 
# files into MongoDB.
#
# TO DO 1: Ingest raw documens from external storage. Here, they come from a folder.
#
# TO DO 2: Separate utility db connection method into a separate module.
#*******************************************************************************

import os
import logging
# import docx2txt
from pathlib import Path
import pymongo

from pymongo import MongoClient

# # Establishing a Connection
# # try:
# #     client = MongoClient('localhost', 27017)
# #     print("Connected successfully!")
# try:
#     client = MongoClient('mongodb://mongodb:27017/')
#     print("Connected successfully form 'mod_convert_docx_to_txt' !")
# except:
#     print("Could not connect to MongoDB")

# # Accessing database
# db = client.edilex_db

# project_dir   = Path(__file__).resolve().parents[2]
# raw_dir       = os.path.join(project_dir, "data/raw")
project_dir   = Path(__file__).resolve().parents[1]
interim_dir   = os.path.join(project_dir, "interim")

def open_connect_mongoDb():
    # Establishing a Connection
    try:
        client = MongoClient('localhost', 27017)
        # client = MongoClient('mongodb://mongodb:27017/')
        print("Connected successfully!")
    except:
        print("Could not connect to MongoDB")

    # Accessing database
    db = client.edilex_db
    return db, client


def close_connect_mongoDb(client):
    client.close()



def convert_docx_files_to_txt():
    """
    Converts Word documents from (../raw) folder into
    text format and saves them in (../interim) directory.
    """
    logger = logging.getLogger(__name__)
    logger.info('MOD_CONVERT_TO_TXT module')

    logger.info('extracting just the contract files into the interim folder')

    for foldername in os.listdir(raw_dir):
        if foldername[0] != ".":
            # print(foldername)
            subdirectory = os.path.join(raw_dir, foldername)

            for filename in os.listdir(subdirectory):
                if filename[0] != ".":
                    if filename.startswith("Contrat"):
                        name, extension = os.path.splitext(filename)
                        new_filename = foldername + extension
                        # print(new_filename)
                        copyfile(os.path.join(subdirectory, filename), os.path.join(interim_dir, new_filename))

    logger.info('converting .docx to .txt')

    for process_file in os.listdir(interim_dir):

        if process_file[0] != ".":
            filename, _ = os.path.splitext(process_file)

            dest_filename = filename[5:].lower()
            dest_filename = ''.join(e for e in dest_filename if e.isalnum() or e == ' ')
            dest_filename = remove_extraneous_space(dest_filename)
            dest_filename = dest_filename.replace(' ', '_')

            dest_filename = dest_filename.replace('compctitif', 'competitif')
            dest_filename = dest_filename.replace('ctapes', 'etapes')
            dest_filename = dest_filename.replace('lics', 'lies')
            dest_filename = dest_filename.replace('cquipement', 'equipement')
            dest_filename = dest_filename.replace('o_la_', '')
            dest_filename = dest_filename + '.txt'
            # print(dest_filename)

            content = docx2txt.process(os.path.join(interim_dir, process_file))
            # Do not remove extraneous spaces - they are needed for breaking text into clauses
            # content = remove_extraneous_space(content)

            new_text_file = open(os.path.join(interim_dir, dest_filename), "w+")
            new_text_file.write(content)
            new_text_file.close()
            os.remove(os.path.join(interim_dir, process_file))


def insert_contracts_into_db():
    """
    Opens interim .txt files one by one and writes them into the 'contracts' collection in MongoDB.
    Collection structure: 
    {
        # "contract_id" : int,
        type : "contract"
        "industry": string,
        "contract_text": string
    }
    """
    logger = logging.getLogger(__name__)
    logger.info('Inserting contracts into the "full_contract_text" collection of "edilex_db" database')

    counter = 0

    for filename in os.listdir(interim_dir):

        if filename[0] != ".":
            with open(os.path.join(interim_dir, filename), 'r+') as f:
                mystr = f.read()

            industry, _ = os.path.splitext(filename)
            counter += 1

            db.full_contract_text.insert_one(
                { 
                    "contract_id" : counter,
                    "industry" : industry,
                    "contract_text" : mystr
                }
            )
    
    # create a unique index on key 'contract_id'
    db.full_contract_text.create_index([('contract_id', pymongo.ASCENDING)], unique=True)