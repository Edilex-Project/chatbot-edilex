# -*- coding: utf-8 -*-

#*******************************************************************************
# Converts raw Word documents (contracts) to vectorized embedded documents stored
# in MongoDB. 
#
# Uncomment the functions after the logger() line and run only once. 
#*******************************************************************************

import logging
from mod_convert_docx_to_txt import *
from mod_split_contracts import *
from mod_clean_vectorize import *

def main():
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')

    # convert_docx_files_to_txt()
    # insert_contracts_into_db()
    # split_contracts_into_sections()
    # split_sections_into_clauses()
    # vectorize_and_save()

    # create_compact_sections()
    clean_and_save()


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # # not used in this stub but often useful for finding various files
    # project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    # load_dotenv(find_dotenv())

    main()
