# -*- coding: utf-8 -*-

#*******************************************************************************
# Converts raw Word documents (contracts) to vectorized embedded documents stored
# in MongoDB. 
#
# It should run only once. 
#*******************************************************************************

import re
import logging
import json
from pymongo import MongoClient
import numpy as np
import pandas as pd
import spacy


def _connect_mongo(host, port, username, password, db):
    """ A utility for making a connection to mongo """

    if username and password:
        mongo_uri = 'mongodb://%s:%s@%s:%s/%s' % (username, password, host, port, db)
        conn = MongoClient(mongo_uri)
    else:
        conn = MongoClient(host, port)
    return conn[db]

def read_mongo(db, collection, query={}, host='localhost', port=27017, username=None, password=None, no_id=True):
    """ Read from Mongo and Store into DataFrame """

    logger = logging.getLogger(__name__)
    logger.info('Extracting dataframe from MongoDB')

    # Connect to MongoDB
    try:
        db = _connect_mongo(host=host, port=port, username=username, password=password, db=db)
        print("Connected successfully from 'mod_clean_vectorize'!")
    except:
        print("Could not connect to MongoDB")

    cursor = db[collection].find(query)

    df =  pd.DataFrame(list(cursor))

    # Delete the _id
    if no_id:
        del df['_id']
    return df

# try:
#     client = MongoClient('mongodb://mongodb:27017/')
#     print("Connected successfully from 'mod_clean_vectorize'!")
# except:
#     print("Could not connect to MongoDB")

# db = client.edilex_db
# cursor = db.clauses.find({})
# if cursor:
#     print("extracted cursor successfully")
#     print(list(cursor))
# else:
#     print("couldn't extract cursor")

# df =  pd.DataFrame(list(cursor))


df = read_mongo(db='edilex_db', collection='compact_sections', query={}) 
nlp = spacy.load('fr_core_news_md', disable=["tagger", "parser", "ner"])
nlp.max_length = 2000000
nlp.Defaults.stop_words |= {"\n","facultative","essentielle", "importante",}


def clean_pipe(doc):
    """Return a list of cleaned documents."""

    text = [token.text for token in doc if not token.is_stop and not token.is_space \
            and not token.is_punct and token.is_alpha and len(token)>2 ]
    return text


def cleaner_pipe(texts):
    """Clean a list of texts."""

    cleaner_pipe = []
    for doc in nlp.pipe(texts, batch_size=20):
        cleaner_pipe.append(clean_pipe(doc))
    return cleaner_pipe


def lemmatize_pipe(doc):
    """Return a list of document's lemmas."""

    lemma_list = [str(tok.lemma_).lower() for tok in doc
                  if tok.is_alpha and tok.text.lower() not in nlp.Defaults.stop_words] 
    return lemma_list


def preprocess_pipe(texts):
    """Lemmatize a list of texts. Return a list of lemmas."""

    preproc_pipe = []
    for doc in nlp.pipe(texts, batch_size=20):
        preproc_pipe.append(lemmatize_pipe(doc))
    return preproc_pipe


def vectorize_pipe(texts):
    "Convert a list of texts (a list of strings) into a list of vectors."

    vectorize_pipe = []
    for doc in nlp.pipe(texts, batch_size=20):
        vectorize_pipe.append(doc.vector)
    return vectorize_pipe 


class CustomEncoder(json.JSONEncoder):
    "Custom encoder to avoid 'Cannot encode object' error when saving to MongoDB"
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(CustomEncoder, self).default(obj)
        

def clean_vectorize(df):
    """
    Clean and vectorize the text of a dataframe column. 
    Return dataframe with an additional 'embeddings' column.
    """

    logger = logging.getLogger(__name__)
    logger.info('Lemmatizing and vectorizing texts')

    df['clean'] = df['text']
    df['clean'] = cleaner_pipe(df['clean'])
    df['clean'] = df['clean'].str.join(' ')
    df['clean'] = preprocess_pipe(df['clean'])
    df['clean'] = df['clean'].str.join(' ')
    # df['embeddings'] = vectorize_pipe(df['clean'])
    # df['embeddings'] = df['embeddings'].tolist()
    return(df)


def vectorize_and_save():
    """Process clauses into embeddings (vectors) and save in 'vectors' db collection"""

    logger = logging.getLogger(__name__)
    logger.info('Creating a db collection with vectors ')

    df_vectorized = clean_vectorize(df)

    # Represent dataframe as dictionary in preparation for saving in MongoDB
    data_dict = df_vectorized.to_dict(orient ='records')
    data_dict_1 = json.dumps(data_dict,cls=CustomEncoder)
    data_dict_final  = json.loads(data_dict_1)

    db = _connect_mongo(host='localhost', port=27017, username=None, password=None, db='edilex_db')
    db.vectors.insert(data_dict_final)



def clean_and_save():
    """Process clauses into embeddings (vectors) and save in 'vectors' db collection"""

    logger = logging.getLogger(__name__)
    logger.info('Creating a db collection with vectors ')

    df_vectorized = clean_vectorize(df)

    # Represent dataframe as dictionary in preparation for saving in MongoDB
    data_dict = df_vectorized.to_dict(orient ='records')
    data_dict_1 = json.dumps(data_dict,cls=CustomEncoder)
    data_dict_final  = json.loads(data_dict_1)

    db = _connect_mongo(host='localhost', port=27017, username=None, password=None, db='edilex_db')
    db.clean_compact_sections.insert(data_dict_final)

