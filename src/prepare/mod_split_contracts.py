# -*- coding: utf-8 -*-

#*******************************************************************************
# This module contains functions to split contracts into sections and 
# to split sections into clauses.  
#*******************************************************************************


import logging
from pymongo import MongoClient

# Establishing a Connection
try:
    client = MongoClient('localhost', 27017)
    # client = MongoClient('mongodb://mongodb:27017/')
    print("Connected successfully from mod_split_contracts")
except:
    print("Could not connect to MongoDB")

db = client.edilex_db


def remove_extraneous_space(text):
    """ Removes extraneous spaces in text. """
    text = text.strip()
    text = text.split()
    return " ".join(text)


def split_contracts_into_sections():
    """
    Reads full contract texts from DB, splits them into sections, and saves the sections in DB. 

    Collection structure: 
    {   
        'type': 'contract',
        'contract_id': contract_id,
        'section_id': <int>,
        'industry': <string>,
        'section': section_title,
        'text': section_text
    }
    """
    logger = logging.getLogger(__name__)
    logger.info('Splitting full contract texts into sections')

    contracts = db.full_contract_text.find()

    for contract in contracts:
        text = contract['contract_text']

        # Section 0
        _, _, right_str = text.partition('INTERPRÉTATION')
        _, _, right_str = right_str.partition('INTERPRÉTATION')

        section_0, _, right_str = right_str.partition('OBJET')
        db.full_sections.insert({
            'type': 'contract',
            'contract_id': contract['contract_id'],
            'section_id': 0,
            'industry': contract['industry'],
            'section': 'interpr',
            'text': section_0
        })

        # Section 1
        section_1, _, right_str = right_str.partition('CONTREPARTIE')
        db.full_sections.insert({
            'type': 'contract',
            'contract_id': contract['contract_id'],
            'section_id': 1,
            'industry': contract['industry'],
            'section': 'objet',
            'text': section_1
        })


        # Section 2
        section_2, _, right_str = right_str.partition('MODALITÉS DE PAIEMENT')
        db.full_sections.insert({
            'type': 'contract',
            'contract_id': contract['contract_id'],
            'section_id': 2,
            'industry': contract['industry'],
            'section': 'contrepartie',
            'text': section_2
        })

        # Section 3
        section_3, _, right_str = right_str.partition('SÛRETÉS')
        db.full_sections.insert({
            'type': 'contract',
            'contract_id': contract['contract_id'],
            'section_id': 3,
            'industry': contract['industry'],
            'section': 'paiement',
            'text': section_3
        })

        # Section 4
        section_4, _, right_str = right_str.partition('ATTESTATIONS RÉCIPROQUES')
        db.full_sections.insert({
            'type': 'contract',
            'contract_id': contract['contract_id'],
            'section_id': 4,
            'industry': contract['industry'],
            'section': 'suretes',
            'text': section_4
        })

        # Section 5
        section_5, _, right_str = right_str.partition('ATTESTATIONS DE L\'ORGANISME PUBLIC')
        db.full_sections.insert({
            'type': 'contract',
            'contract_id': contract['contract_id'],
            'section_id': 5,
            'industry': contract['industry'],
            'section': 'attest_recipr',
            'text': section_5
        })

        # Section 6
        section_6, _, right_str = right_str.partition('ATTESTATIONS DU FOURNISSEUR/PRESTATAIRE DE SERVICES/ENTREPRENEUR')
        db.full_sections.insert({
            'type': 'contract',
            'contract_id': contract['contract_id'],
            'section_id': 6,
            'industry': contract['industry'],
            'section': 'attest_org_public',
            'text': section_6
        })

        # Section 7
        section_7, _, right_str = right_str.partition('OBLIGATION(S) RÉCIPROQUE(S)')
        db.full_sections.insert({
            'type': 'contract',
            'contract_id': contract['contract_id'],
            'section_id': 7,
            'industry': contract['industry'],
            'section': 'attest_fourniss',
            'text': section_7
        })

        # Section 8
        section_8, _, right_str = right_str.partition('OBLIGATIONS DE L\'ORGANISME PUBLIC')
        db.full_sections.insert({
            'type': 'contract',
            'contract_id': contract['contract_id'],
            'section_id': 8,
            'industry': contract['industry'],
            'section': 'oblig_recipr',
            'text': section_8
        })

        # Section 9
        section_9, _, right_str = right_str.partition('OBLIGATIONS DU FOURNISSEUR/PRESTATAIRE DE SERVICES/ENTREPRENEUR')
        db.full_sections.insert({
            'type': 'contract',
            'contract_id': contract['contract_id'],
            'section_id': 9,
            'industry': contract['industry'],
            'section': 'oblig_org_public',
            'text': section_9
        })

        # Section 10
        section_10, _, right_str = right_str.partition('DISPOSITIONS PARTICULIÈRES')
        db.full_sections.insert({
            'type': 'contract',
            'contract_id': contract['contract_id'],
            'section_id': 10,
            'industry': contract['industry'],
            'section': 'oblig_fourniss',
            'text': section_10
        })

        # Section 11
        section_11, _, right_str = right_str.partition('DISPOSITIONS GÉNÉRALES')
        db.full_sections.insert({
            'type': 'contract',
            'contract_id': contract['contract_id'],
            'section_id': 11,
            'industry': contract['industry'],
            'section': 'dispo_partic',
            'text': section_11
        })

        # Section 12
        section_12, _, right_str = right_str.partition('FIN DU CONTRAT')
        db.full_sections.insert({
            'type': 'contract',
            'contract_id': contract['contract_id'],
            'section_id': 12,
            'industry': contract['industry'],
            'section': 'dispo_generales',
            'text': section_12
        })

        # Section 13
        section_13, _, right_str = right_str.partition('ENTRÉE EN VIGUEUR')
        db.full_sections.insert({
            'type': 'contract',
            'contract_id': contract['contract_id'],
            'section_id': 13,
            'industry': contract['industry'],
            'section': 'fin',
            'text': section_13
        })

        # Section 14
        section_14, _, right_str = right_str.partition('DURÉE')
        db.full_sections.insert({
            'type': 'contract',
            'contract_id': contract['contract_id'],
            'section_id': 14,
            'industry': contract['industry'],
            'section': 'entree_en_vigueur',
            'text': section_14
        })

        # Section 15
        section_15, _, right_str = right_str.partition('PORTÉE')
        db.full_sections.insert({
            'type': 'contract',
            'contract_id': contract['contract_id'],
            'section_id': 15,
            'industry': contract['industry'],
            'section': 'duree',
            'text': section_15
        })

        # Section 16
        section_16, _, right_str = right_str.partition('ANNEXE A')
        db.full_sections.insert({
            'type': 'contract',
            'contract_id': contract['contract_id'],
            'section_id': 16,
            'industry': contract['industry'],
            'section': 'portee',
            'text': section_16
        })


def split_sections_into_clauses():
    """
    Reads contract sections from DB, splits them into clauses, and saves the clauses in DB. 

    Collection 'clauses' structure: 
    {
        'clause_id': int,
        'type': 'contract',
        'contract_id': contract_id <contract key>,
        'section_id': section_id,
        'industry': string,
        'section': section_title,
        'text': clause_text
    }
    """
    logger = logging.getLogger(__name__)
    logger.info('Splitting sections into clauses')

    counter = 0

    sections = db.full_sections.find()

    for section in sections:
        clauses = section['text']
        clauses = clauses.splitlines()
        # remove empty elements from the list
        clauses = [i for i in clauses if i] 
        for clause in clauses:
            counter += 1
            db.clauses.insert({
                'clause_id': counter,
                'type': 'contract',
                'contract_id': section['contract_id'],
                'section_id': section['section_id'],
                'industry': section['industry'],
                'section': section['section'],
                'text': clause
            })
        
def create_compact_sections():
    """
    Reads contract sections from DB, removes unnecessary spaces, and inserts into compact_sections collection. 

    Collection 'compact_sections' structure: 
    {
        'type': 'contract',
        'contract_id': contract_id <contract key>,
        'section_id': section_id,
        'industry': string,
        'section': section_title,
        'text': clause_text
    }
    """
    logger = logging.getLogger(__name__)
    logger.info('Compacting sections!')

    sections = db.full_sections.find()

    for section in sections:
        compact_section = remove_extraneous_space(section['text'])
        db.compact_sections.insert({
            'type': 'contract',
            'contract_id': section['contract_id'],
            'section_id': section['section_id'],
            'industry': section['industry'],
            'section': section['section'],
            'text': compact_section
        })
    
    # create a unique index on key 'clause_id'
    # https://pymongo.readthedocs.io/en/stable/tutorial.html
    # db.clauses.create_index([('clause_id', pymongo.ASCENDING)], unique=True)

