# 1. Set up the OS and the source language
#--------------------------------------------------------

FROM ubuntu:latest
RUN apt-get update
RUN apt-get install -y build-essential python3.6 python3-pip python3-dev
RUN pip3 install --upgrade pip

# Use existing image that already has Conda installed and set up
FROM continuumio/miniconda3:latest


# 2. Create a /src working directory and copy everything into it
#---------------------------------------------------------------

RUN mkdir src
WORKDIR src/
COPY . .

# 3. Install required libraries
#-------------------------------

# Create Conda environment from .yml file
RUN conda update -n base -c defaults conda
RUN conda env create -f environment.yml

# Activate Conda environment
RUN echo "conda activate edilex" > ~/.bashrc
ENV PATH /opt/conda/envs/edilex/bin:$PATH

# Inside the conda environment, install required pip packages
RUN pip3 install -r req.txt 
RUN python -m spacy download fr_core_news_md
RUN pip3 install jupyter --ignore-installed 


# 4. Run module.py to prepare the data
#---------------------------------------

# RUN python3 mod_convert_docx_to_text.py


# 5. Remove the raw data from the Docker image
#----------------------------------------------

# RUN rm -rf /src/data


# 6. Change to /notebooks directory
#----------------------------------

WORKDIR /src/notebooks


# 7. This avoids crashes (from Jupyter Docker Stacks project)
#-------------------------------------------------------------

# Add Tini. Tini operates as a process subreaper for jupyter. This prevents kernel crashes.
ENV TINI_VERSION v0.6.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /usr/bin/tini
RUN chmod +x /usr/bin/tini
ENTRYPOINT ["/usr/bin/tini", "--"]


# 8. Finally, start up the Jupyter Notebook in the container
#------------------------------------------------------------

CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root"]

