# -*- coding: utf-8 -*-

import re
import logging
import json
from pymongo import MongoClient
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, IntegerField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, NumberRange

#*******************************************************************************
# This module contains utility classes and functions used by app.py
#*******************************************************************************


class InfoForm(FlaskForm):
    industry = SelectField(u'Industry: ',
            choices=[
                ('biens', 'Biens (Fournitures)'),
                ('biens_informatiques_equipement', 'Biens informatiques (Équipement)'),
                ('biens_informatiques_equipement_2_etapes', 'Biens informatiques (Équipement) - 2 étapes'),
                ('biens_informatiques_equipement_dialogue_competitif_2_etapes', 'Biens informatiques (Équipement) - dialogue compétitif - 2 étapes'),
                ('biens_informatiques_logiciels', 'Biens informatiques (logiciel)'),
                ('biens_informatiques_logiciel_2_etapes', 'Biens informatiques (logiciel) - 2 étapes'),
                ('biens_informatiques_logiciel_dialogue_competitif_2_etapes', 'Biens informatiques (logiciel) - dialogue compétitif - 2 étapes'),
                ('biens_techniques', 'Biens techniques'),
                ('construction', 'Construction'),
                ('construction_2_etapes', 'Construction - 2 étapes'),
                ('construction_et_services_professionnels', 'Construction et services professionnels'),
                ('construction_et_services_profesionnels_2_etapes', 'Construction et services professionnels - 2 étapes'),
                ('haute-technologie', 'Haute technologie'),
                ('services_informatiques', 'Services informatiques'),
                ('services_informatiques_2_etapes', 'Services informatiques - 2 étapes'),
                ('services_informatiques_dialogue_competitif_2_etapes', 'Services informatiques - dialogue compétitif - 2 étapes'),
                ('services_professionnels', 'Services professionnels'),
                ('services_professionnels_lies_construction', 'Services professionnels liés à la construction'),
                ('services_techniques', 'Services techniques')
                ],
                validators=[DataRequired()])

    type_of_doc = SelectField(u'Type of document: ',
            choices=[
                ('contrat', 'Contrat'),
                ],
                validators=[DataRequired()])

    section = SelectField(u'Section: ',
            choices=[
                ('interpr', 'Interprétation'),
                ('objet', 'Objet'),
                ('contrepartie', 'Contrepartie'),
                ('paiement', 'Modalités de paiement'),
                ('suretes', 'Sûretés'),
                ('attest_recipr', 'Attestations réciproques'),
                ('attest_org_public', 'Attestations de l\'organisme public'),
                ('attest_fourniss', 'Attestation du fournisser/prestataire de services/entrepreneur'),
                ('oblig_recipr', 'Obligation(s) réciproque(s)'),
                ('oblig_org_public', 'Obligations de l\'organisme public'),
                ('oblig_fourniss', 'Obligations du fournisseur/prestatire de services/entrepreneur'),
                ('dispo_partic', 'Dispositions particulières'),
                ('dispo_generales', 'Dispositions générales'),
                ('fin', 'Fin du contrat'),
                ('entree_en_vigueur', 'Entrée en vigueur'),
                ('duree', 'Durée'),
                ('portee', 'Portée')
                ],
                validators=[DataRequired()])

    question = TextAreaField(u'Your question: ', validators=[DataRequired()])

    how_many = SelectField(u'Maximum number of results: ',
                choices=[
                    ('1', '1'),
                    ('2', '2'),
                    ('3', '3'),
                    ('4', '4'),
                    ('5', '5'),
                    ('6', '6'),
                    ('7', '7'),
                    ('8', '8'),
                    ('9', '9'),
                    ('10', '10'),
                    ],
                    validators=[DataRequired()])

    submit = SubmitField("Submit")


class CustomEncoder(json.JSONEncoder):
    "Custom encoder to avoid 'Cannot encode object' error when saving to MongoDB"
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(CustomEncoder, self).default(obj)

def open_connect_mongoDb():
    # Establishing a Connection
    try:
        # use this connection when working locally, with locally installed MongoDB
        # client = MongoClient('localhost', 27017)
        # use this connection with container MongoDB
        client = MongoClient('mongodb://mongodb:27017/')

        print("Connected to MongoDB successfully on the backend!")
    except:
        print("Could not connect to MongoDB")
    
    # Accessing the database
    db = client.edilex_db

    return db, client


def close_connect_mongoDb(client):
    client.close()


def clean(doc):
    """
    Breaks document into tokens, cleans them, de-tokenizes back into clean doc.
    """
    text = [tok.text for tok in doc if not tok.is_stop and not tok.is_space \
            and not tok.is_punct and tok.is_alpha and len(tok)>2 ]

    return ' '.join(text)


def lemmatize(doc):
    """
    Breaks document into tokens, lemmatizes them, de-tokenizes back into lemmatized doc.
    """
    lemma_list = [str(tok.lemma_).lower() for tok in doc
                  if tok.is_alpha] 

    return ' '.join(lemma_list)


def convert_to_dict(user_request):
    """
    Converts use
    """
    # MongoDB doesn't save numpy arrays, they need to be transformed to lists
    # with the help of our CustomEncoder class.
    data_dict_1 = json.dumps(user_request, cls=CustomEncoder)
    data_dict_final  = json.loads(data_dict_1)

    return data_dict_final


def most_similar(doc, doc_list, topn=5):
    '''
    Calculates similarity between a target document and each document in the list, 
    sorts in descending order and return N top most similar documents

    '''
    by_similarity = sorted(doc_list, key = lambda d: doc.similarity(d), reverse=True)
    return [(d, d.similarity(doc)) for d in by_similarity[:topn] ]
