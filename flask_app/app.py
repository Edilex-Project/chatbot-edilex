# -*- coding: utf-8 -*-
__author__ = 'Elena Kolmogorova, December 2020'
#*******************************************************************************
# This is the front-end app for Edilex project.
# Run it with 'python app.py' command.
# It shows a form and connects to MongoDB database to get results. 
# Pre-requisits:
# - install Flask library.
#*******************************************************************************

from backend import *
import spacy

from flask import Flask, render_template, session, url_for, request, redirect

app = Flask(__name__)

app.config['SECRET_KEY'] = 'mysecretkey'

# Make Spacy's NLP model global.
model=None
def load_model():
    global nlp
    nlp = spacy.load('fr_core_news_md', disable=["tagger", "parser", "ner"])


def main():

    @app.route('/', methods=['GET', 'POST'])
    def index():

        form = InfoForm()

        if form.validate_on_submit():

            # capture user request
            session['industry'] = form.industry.data
            session['type_of_doc'] = form.type_of_doc.data
            session['section'] = form.section.data
            session['question'] = form.question.data
            session['how_many'] = form.how_many.data

            # clean and lemmatize user request
            doc = nlp(form.question.data)
            clean_doc = clean(doc)
            lemmatized_doc = lemmatize(nlp(clean_doc))
            user_req = nlp(lemmatized_doc)

            # save max number of answers the user wants to see
            max_res = int(form.how_many.data)
            
            # save user request in db
            db, client = open_connect_mongoDb()

            print

            request = {
                'industry': form.industry.data,
                'type_of_doc': form.type_of_doc.data,
                'section': form.section.data,
                'how_many': form.how_many.data,
                'raw_request': form.question.data,
                'clean_request': lemmatized_doc
            }
            db.user_request.insert(convert_to_dict(request))

            # get requested data from db 
            cursor = db.clean_compact_sections.find( { 'industry': request['industry']},  {'industry':1, 'section': 1, 'text': 1, 'clean':1, '_id':1} )

            # calculate similarity between user request and KB
            clean_docs = []

            for item in cursor:
                process_item = nlp(item['clean'])
                clean_docs.append(process_item)

            # clean_docs = [nlp(doc['clean']) for doc in cursor]
            topN = most_similar(user_req, clean_docs, max_res)

            clauses = []
            for doc, similarity in topN:
                print(similarity)
                clauses.append((doc[:100], similarity))
            
            close_connect_mongoDb(client)

            return render_template('index.html', form=form, clauses=clauses, max_res=max_res)

        return render_template('index.html', form=form)

if __name__ == '__main__':
    print(("* Loading Spacy's French NLP model and starting Flask server..."
        "please wait until server has fully started"))
    load_model()
    main()
    app.run(debug=True, host='0.0.0.0',port=4000)
